import { useState } from "react";
import { Todo } from "../types/todoList";

const useTodoList = ()=>{
  const [todos, setTodos] = useState<Todo[]>([])

  // add a new record
  const addTodo = (todo:Todo)=>{
    setTodos([
      ...todos,
      todo,
    ]);
  };

  // toggle todo state between done or not done
  const switchTodoDoneStatus = (id:number)=>{
    const targetTodoIndex = todos.findIndex(
      (todo:Todo)=>( todo.id === id )
    );
    const newTodos = [...todos]

    newTodos[targetTodoIndex]={
      ...newTodos[targetTodoIndex],
      done: !newTodos[targetTodoIndex].done,
    };
    setTodos(newTodos);
  }

  // use filter remove a record and replace with the news array
  const deleteTodo = (id:number)=>{
    const newTodos = todos.filter((todo:Todo)=>( todo.id !== id ));
    setTodos(newTodos);
  };

  // filter To do List: Hide done itmes
  const [filterDoneTodo, setFilterdoneTodo] = useState<boolean>(false);
  const switchFilterDoneTodo = ()=>{
    setFilterdoneTodo(!filterDoneTodo);
  }
  let workTodos = todos;
  if(filterDoneTodo){
    workTodos = todos.filter( todo => !todo.done );
  }


  return { 
    todos: workTodos, 
    addTodo, 
    switchTodoDoneStatus, 
    deleteTodo,
    filterDoneTodo,
    switchFilterDoneTodo,
  } as const;
}

export default useTodoList;