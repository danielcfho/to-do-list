import React, { useState, useEffect } from 'react'
import { Todo } from '../types/todoList'
import styles from './index.module.scss'

type FormProps = {
  addTodo: (todo:Todo)=>void
}

const Form = (props:FormProps)=>{
  const [name, setName] = useState<string>('')
  const [msg,setMsg] = useState<string>("Input an Item")

  const submitForm = ()=>{
    const newTodo = {id:Math.random(),name,done:false}
    if (name != '') {
      props.addTodo(newTodo)
      setName('')
      setMsg('Input an Item')
    }else{
      setMsg('Type some in the item')
    }
  }
  return(
    <div className={styles.form}>
      <span>{ msg }</span>
      <div>
        <input
          type="input"
          className={ styles.todoInput }
          value={ name }
          onChange={ (e)=>{ setName(e.target.value) } }
        />
        <button
          type="button"
          className={ styles.submitBtn }
          onClick={ submitForm }
        >ADD</button>
      </div>
    </div>
  )
}

export default Form