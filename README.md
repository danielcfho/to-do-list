# My to-do-app



This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Installation

```bash
yarn create react-app . --template typescript
```
# 1. Design
## Design UI Reference: 
[Todo List from codepen](https://codepen.io/artuphays/pen/qMeXzN)


# 2. Build up Customs Hooks
## 2.1 Folder Organization
```
src
├── hooks
│   └── useTodoList.ts
├── types
│   └── todoList.ts
├── index.tsx
```

## 2.2 Data Structure

/src/types/todoList.ts
```ts
export interface ITodo{
  id: number
  done: bollean
  name: string
}
```

/src/hooks/useTodoList.ts:
```ts
import { useState } from "react";
import { Todo } from "../types/todoList";

const useTodoList = ()=>{
  const [todos, setTodos] = useState<Todo[]>([])
  return { todos } as const;
}

export default useTodoList;
```

# 3. Logic Event according to the view

## 3.1 Add a new record

add addTodo method into hooks:
/src/hooks/useTodoList.ts
```ts
...
const useTodoList = ()=>{
  const [todos, setTodos] = useState<Todo[]>([])

  const addTodo = (todo:Todo)=>{
    setTodos([
      ...todos,
      todo,
    ]);
  };

  return { todos } as const;
}
...
```
*remember you should use (new) record, not the `Array.prototype.push`*


## 3.2 change todo record stats
- using `array.findIndex()` on state in React
- this time we needs make record for 2 times. first is all todolist array, another is the todo record we need to change the states.

/src/hooks/useTodoList.ts
```ts
const useTodoList =()=> {
  ...
  // toggle todo state between done or not done
  const switchTodoDoneStatus = (id:number)=>{
    const targetTodoIndex = todos.findIndex(
      (todo:Todo)=>( todo.id === id )
    );

    const newTodos = [...todos]

    newTodos[targetTodoIndex]={
      ...newTodos[targetTodoIndex],
      done: !newTodos[targetTodoIndex].done,
    };

    setTodos(newTodos);
  }

  return { todos, addTodo, switchTodoDoneStatus } as const;
}
```



## 3.3 delete a record
- use `Array.prototype.filter` to delete a record of doto. and return a new todolist array to replace the old one:

/src/hooks/useTodoList.ts
```ts
const useTodoList()=>{
  ...
  // use filter remove a record and replace with the news array
  const deleteTodo = (id:number)=>{
    const newTodos = todos.filter((todo:Todo)=>( todo.id !== id ));
    setTodos(newTodos);
  };

  return { todos, addTodo, switchTodoDoneStatus, deleteTodo } as const;
}
```

## 3.4 filter to-do list items according to the record state
- is the filter on or off?
- is there having status with doto filter?

/src/hooks/useTodoList.ts
```ts
const useTodoList = ()=>{
  ...
  // use filter remove a record and replace with the news array
  const deleteTodo = (id:number)=>{
    const newTodos = todos.filter((todo:Todo)=>{
      todo.id !== id
    });
    setTodos(newTodos);
  };

  // filter To do List: Hide done itmes
  const [filterDoneTodo, setFilterdoneTodo] = useState<boolean>(false);
  const switchFilterDoneTodo = ()=>{
    setFilterdoneTodo(!filterDoneTodo);
  }
  let workTodos = todos;
  if(filterDoneTodo){
    workTodos = todos.filter( todo => !todo.done );
  }


  return { 
    todos: workTodos, 
    addTodo, 
    switchTodoDoneStatus, 
    deleteTodo,
    filterDoneTodo,
    switchFilterDoneTodo,
  } as const;
}
```

> remember: use new date to update new record if its is `Array` or `Object`

# 4. views components
- create a components directory
- add TodoList component:

/src/components/TodoList.tsx:
```ts
import React from 'react'

const TodoList = ()=>{
  return(
    <div>
      TodoList
    </div>
  )
}

export default TodoList;
```

## 4.1 Slice the view to components
![TodoList View](/slice.png)

- the first slice span is the "Hide done items" item.
- single todo item
- the view that place all todo items.
- last, the bottom form that can add a single todo item.

### 4.1.1 Basic style
#### Problem
! Cannot find module './index.scss' or its corresponding type declarations.
[ref:](https://cloud.tencent.com/developer/article/1866100)

#### Resolve:
create /src/react-app-env.d.ts:
```ts
/// <reference types="react-scripts" />
declare module '*.module.css' {
  const classes:{
    readonly[key:string]:string
  }
  export default classes
}

declare module '*.module.scss' {
  const classes:{
    readonly[key:string]:string
  }
  export default classes
}

declare module '*.module.sass' {
  const classes:{
    readonly[key:string]:string
  }
  export default classes
}
```
now you can `import styles from './index.module.scss'

## 4.2 Layout:
index.modules.scss:
```scss
.layout{
  display:flex;
  justify-content: center;
  align-items: center;
  width:100%;
  height:100%;
}

.todoListWrapper{
  display:flex;
  flex-direction: column;
  width: 400px;
  height: 500px;
  border: 1px solid #000000;
}
```

TodoList.tsx:
```ts
...
import style from "./index.module.scss";

const TodoList = () => {
  return (
    <div className={style.layout}>
      <div className={style.todoListWrapper}>
        TodoList
      </div>
    </div>
  );
};
...
```

## 4.3 Title
index.modules.scss:
```scss
.todoListWrapper{
  ...
  .todoListWrapper{
    display:flex;
    flex-direction: column;
    width: 400px;
    height: 500px;
    border: 1px solid #000000;

    .header{
      display:flex;
      flex-direction: column;
      margin: 24px;

      .title{
        font-size: 32px;
      }

      .subTitle{
        font-size: 20px;
      }
    }
...
}
```

TodoList.tsx:
```ts
const TodoList = () => {
  return (
    <div className={styles.layout}>
      <div className={styles.todoListWrapper}>
        <div className={styles.header}>
          <span className="style title">TodoList</span>
          <span className={styles.subTitle}>Sample todo list</span>
        </div>
      </div>
    </div>
  );
};
```

## 4.4 ToDo List Filter Toggle
### 4.4.1 Style:
index.modules.scss:
```scss
.filter{
  display: flex;
  justify-content: flex-end;
  margin: 0px 24px;
}
```

make Filter component:
/src/components/Filter.tsx:
```ts
import React from 'react'
import styles from './index.module.scss'

const Filter = ()=>{
  return (
    <div className={styles.filter}>
      <input type="checkbox" />
      Hide done items:
    </div>
  )
}

export default Filter;
```
add Filter into TodoList.tsx:
```ts
const TodoList = () => {
  return (
    <div className={styles.layout}>
      <div className={styles.todoListWrapper}>
       ... 
        <Filter />
      </div>
    </div>
  );
};
...
```

### 4.4.2 Use Hook:
add hook into TodoList.tsx:
```ts
import useTodoList from '../hooks/useTodoList';
...
const TodoList = () => {
  const todoList = useTodoList();
       ... 
        <Filter 
          filterDoneTodo={ todoList.filterDoneTodo }
          switchFilterDoneTodo= { todoList.switchFilterDoneTodo }
        />
    ...
```

and add props into Filter.tsx:
```ts
type FilterProps={
  filterDoneTodo:boolean;
  switchFilterDoneTodo:()=>void;
}

const Filter = (props:FilterProps)=>{
  return (
    <div className={styles.filter}>
      <input type="checkbox" 
        checked={ props.filterDoneTodo }
        onChange={ props.switchFilterDoneTodo }
      />
      Hide done items:
    </div>
  )
}
```
## 4.5 Render todo List:
### 4.5.1 TodoItem component
/src/components/TodoItem.tsx:
```ts
import React from 'react'
import { Todo } from '../types/todoList'

type TodoProps = {
  todo: Todo;
  switchTodoDoneStatus:(id:number)=>void;
  deleteTodo:(id:number)=>void;
}

const TodoItem = (props:TodoProps)=>{
  return (
    <div>
      <span >{ props.todo.name }</span>
      <div>
        <input
          type="checkbox"
          checked={props.todo.done}
          onChange={ ()=>{ props.switchTodoDoneStatus(props.todo.id) } }
        />
        <button
          type="button"
          onClick={ ()=>{ props.deleteTodo(props.todo.id) } }
        >Delete</button>
      </div>
    </div>
  )
}

export default TodoProps
```

### 4.5.2 put TodoIem into TodoList.tsx:
```ts
...
import TodoItem from '../components/TodoItem'
...
<div>
{
  todoList.todos.map(todo=>(
    <TodoItem
      key={todo.id}
      todo={todo}
      switchTodoDoneStatus={todoList.switchTodoDoneStatus}
      deleteTodo={todoList.deleteTodo}
    />
  ))
}
</div>
...
```

## 4.6 Input Form:
### 4.6.1 Style:
index.module.scss:
```ts
...
$fontInputSize: 16px;
$formElementHeight:50px;

.form{
  margin: $margin;

  .todoInput{
    width: 280px;
    height: $formElementHeight;
    font-size: $fontInputSize;
  }

  .submitBtn{
    width: 72px;
    height: $formElementHeight;
    font-size: $fontInputSize;
  }
}
...
```

### 4.6.2 Make a Input Form Component:
- Form has his own state for use input
- submitForm function for button click
- `submitForm()` make a new item with prop

/src/components/Form.tsx:
```ts
import React, { useState } from 'react'
import { Todo } from '../types/todoList'
import styles from './index.module.scss'

type FormProps = {
  addTodo: (todo:Todo)=>void
}

const Form = (props:FormProps)=>{
  const [name, setName] = useState<string>('')
  const submitForm = ()=>{
    const newTodo = {id:Math.random(),name,done:false}
    props.addTodo(newTodo)
    setName('')
  }
  return(
    <div className={styles.form}>
      <span>Add to the todo list</span>
      <div>
        <input
          type="input"
          className={ styles.todoInput }
          value={ name }
          onChange={ (e)=>{ setName(e.target.value) } }
        />
        <button
          type="button"
          className={ styles.submitBtn }
          onClick={ submitForm }
        >ADD</button>
      </div>
    </div>
  )
}

export default Form
```

### 4.6.3 Put Form into TodoList:
TodoList.tsx:
```ts
...
import Form from './Form';
...
<Form addTodo={ todoList.addTodo } />
...
```

## 4.7 Adject Style Sheet

### 4.7.1 item style
index.module.scss:
```scss
...
.todoList{
  height: 300px;
  overflow: scroll;

  .todoItem{
    padding: 0px 24px;
    margin: 4px 0px;
    background: #c9c9c9;
    color: #ffffff;
    height: 50px;
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
}
```
and put it into TodoList.tsx and TodoItem.tsx

### 4.7.2 done item line-through style
TodoItem.tsx:
```ts
...
<span 
  style={{
    textDecoration: props.todo.done ? 'line-through' : 'none',
  }}
>{ props.todo.name }</span>
...
```